package com.example.lucas.teste_ioasys.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.lucas.teste_ioasys.entity.Enterprise;

import java.util.List;

import com.example.lucas.teste_ioasys.R;

public class ListaEnterpriseAdapter extends ArrayAdapter<Enterprise> {
    private Context context;
    private List<Enterprise> listaEnterprise;
    private static LayoutInflater inflater = null;

    public ListaEnterpriseAdapter(Context context, int textViewResourceId, List<Enterprise> listaEnterprise) {
        super(context, 0, listaEnterprise);
        this.context = context;
        this.listaEnterprise = listaEnterprise;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Enterprise enterprise = this.listaEnterprise.get(position);

        convertView = LayoutInflater.from(this.context).inflate(R.layout.adapter_lista_enterprise, null);

        TextView textNome = (TextView) convertView.findViewById(R.id.textNome);
        TextView textCategory = (TextView) convertView.findViewById(R.id.textCategory);
        TextView textCountry = (TextView) convertView.findViewById(R.id.textCountry);

        textNome.setText(enterprise.getEnterpriseName());
        textCategory.setText(enterprise.getEnterpriseType().getEnterpriseTypeName());
        textCountry.setText(enterprise.getCountry());

        return convertView;
    }
}
