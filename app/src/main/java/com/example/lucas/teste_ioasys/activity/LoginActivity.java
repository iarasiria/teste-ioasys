package com.example.lucas.teste_ioasys.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lucas.teste_ioasys.R;
import com.example.lucas.teste_ioasys.entity.Credentials;
import com.example.lucas.teste_ioasys.entity.Enterprise;
import com.example.lucas.teste_ioasys.entity.EnterpriseList;
import com.example.lucas.teste_ioasys.entity.EnterprisePayload;
import com.example.lucas.teste_ioasys.entity.UserPayload;
import com.example.lucas.teste_ioasys.network.APIServices;
import com.example.lucas.teste_ioasys.network.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private APIServices mAPIServices;
    private AutoCompleteTextView email;
    private EditText password;
    private Button sign_in_btn;

    private static final String MY_PREFS_NAME = "AuthHeaders";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().hide();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.rgb(181, 180,166));
        }


        mAPIServices = RetrofitClient.getAPIService();

        email = (AutoCompleteTextView) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        sign_in_btn = (Button)findViewById(R.id.email_sign_in_button);

        sign_in_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Credentials credentials = new Credentials(email.getText().toString(), password.getText().toString());
                Log.i("credentials", "email: " + email.getText().toString());
                Log.i("credentials", "senha: " + password.getText().toString());

                mAPIServices.authorizeUser(credentials).enqueue(new Callback<UserPayload>() {
                    @Override
                    public void onResponse(Call<UserPayload> call, Response<UserPayload> response) {

                        if (response.code() == 200) {
                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                            editor.putString("uid", response.headers().get("uid"));
                            editor.putString("client", response.headers().get("client"));
                            editor.putString("access-token", response.headers().get("access-token"));
                            editor.commit();

                            Intent enterprisesList = new Intent(LoginActivity.this, HomeActivity.class);
                            LoginActivity.this.startActivity(enterprisesList);
                        }

                    }

                    @Override
                    public void onFailure(Call<UserPayload> call, Throwable t) {
                        Toast.makeText(LoginActivity.this, "Usuário ou senha inválidos", Toast.LENGTH_SHORT).show();

                    }
                });


            }
        });




    }
}
