package com.example.lucas.teste_ioasys.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.lucas.teste_ioasys.R;
import com.example.lucas.teste_ioasys.adapter.ListaEnterpriseAdapter;
import com.example.lucas.teste_ioasys.entity.Enterprise;
import com.example.lucas.teste_ioasys.entity.EnterpriseList;
import com.example.lucas.teste_ioasys.network.*;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {
    private APIServices mAPIServices;
    private ListView listEnterprises;
    private static final String MY_PREFS_NAME = "AuthHeaders";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar);


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
//        getSupportActionBar().setLogo(R.mipmap.logo_nav);
        getSupportActionBar().setDisplayUseLogoEnabled(true);


        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String accessToken = prefs.getString("access-token", null);
        String client = prefs.getString("client", null);
        String uid = prefs.getString("uid", null);

        listEnterprises = (ListView) findViewById(R.id.enterprise_list);
        listEnterprises.setDivider(null);


        mAPIServices = RetrofitClient.getAPIService();


        mAPIServices.getEnterprises(accessToken, client, uid).enqueue(new Callback<EnterpriseList>() {
            @Override
            public void onResponse(Call<EnterpriseList> call, Response<EnterpriseList> response) {
                final EnterpriseList list = response.body();

                ListaEnterpriseAdapter adapter = new ListaEnterpriseAdapter(HomeActivity.this, R.layout.adapter_lista_enterprise, list.getEnterprises());
                listEnterprises.setAdapter(adapter);


                listEnterprises.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {

                        Intent intent = new Intent(HomeActivity.this, ResultadoActivity.class);
                        int enterpriseID = list.getEnterprises().get(position).getId();
                        intent.putExtra("ID", enterpriseID);
                        startActivity(intent);
                    }
                });

            }

            @Override
            public void onFailure(Call<EnterpriseList> call, Throwable t) {
                Toast.makeText(HomeActivity.this, "Sem dados a serem exibidos", Toast.LENGTH_SHORT).show();
            }
        });

    }




}
