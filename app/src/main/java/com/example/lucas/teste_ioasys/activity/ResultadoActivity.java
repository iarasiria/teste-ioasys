package com.example.lucas.teste_ioasys.activity;
import com.example.lucas.teste_ioasys.R;
import com.example.lucas.teste_ioasys.entity.EnterprisePayload;
import com.example.lucas.teste_ioasys.network.APIServices;
import com.example.lucas.teste_ioasys.network.RetrofitClient;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResultadoActivity extends AppCompatActivity {
    private APIServices mAPIServices;
    private TextView description;
    private static final String MY_PREFS_NAME = "AuthHeaders";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String accessToken = prefs.getString("access-token", null);
        String client = prefs.getString("client", null);
        String uid = prefs.getString("uid", null);

        description = (TextView) findViewById(R.id.textView2);

        int enterpriseID = getIntent().getIntExtra("ID", 0);

        mAPIServices = RetrofitClient.getAPIService();

        mAPIServices.getEnterprise(accessToken, client, uid, enterpriseID).enqueue(new Callback<EnterprisePayload>() {
            @Override
            public void onResponse(Call<EnterprisePayload> call, Response<EnterprisePayload> response) {
                EnterprisePayload list = response.body();
                setTitle(list.getEnterprise().getEnterpriseName());
                description.setText(list.getEnterprise().getDescription());
            }

            @Override
            public void onFailure(Call<EnterprisePayload> call, Throwable t) {

            }
        });

    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }



}
